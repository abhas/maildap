// Copyright 2019 Silke Hofstra
//
// Licensed under the EUPL (the "Licence");
//
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and
// limitations under the Licence.

package mail

import (
	"crypto/tls"
	"fmt"
	"log"
	"net"
	"time"

	"github.com/emersion/go-smtp"
)

// Client represents an SMTP client.
type Client struct {
	SMTPServer string
	conn       *smtp.Client
}

// NewClient configures an SMTP client.
func NewClient(server string) (s *Client, err error) {
	return &Client{
		SMTPServer: server,
	}, nil
}

// Worker sends emails that are placed in a channel.
// It automatically connects and disconnects to
func (c *Client) Worker(ch chan *SingleMessage) (err error) {
	for {
		select {
		// Handle message from the channel
		case msg, ok := <-ch:
			// Channel was closed
			if !ok {
				return
			}

			// Send message
			if err = c.SendMessage(msg); err != nil {
				// Handle the error
				drop, err := c.handleSendError(err)

				// Place message back in the queue if possible
				if !drop {
					select {
					case ch <- msg:
						drop = false
					default:
						drop = true
					}
				}

				// Log the error
				log.Printf("Error sending message: %s - Dropped: %v - From: %q - To: %q - Message ID: %q",
					err, drop,
					msg.Message.Header.Get("From"),
					msg.Message.Header.Get("To"),
					msg.Message.Header.Get("Message-ID"))
			}

		// Close connection after a timeout of 30 seconds
		case <-time.After(30 * time.Second):
			if err = c.Disconnect(); err != nil {
				return
			}
		}
	}
}

// handleSendError handles an error that occurred when sending a message.
func (c *Client) handleSendError(msgErr error) (drop bool, err error) {
	err = msgErr

	// Check if the error is an SMTP error
	if smtpErr, ok := parseSMTPError(err); ok {
		// Drop message on permanent errors
		if smtpErr.EnhancedCode[0] == 5 {
			drop = true
		}
	} else {
		// Disconnect to force a reconnect
		if disconErr := c.Disconnect(); disconErr != nil {
			err = fmt.Errorf("disconnect error %q while handling %q", disconErr, err)
		}
	}

	return
}

// Connect connects to the email server.
func (c *Client) Connect() (err error) {
	// Only connect once
	if c.conn != nil {
		return
	}

	// Connect to server
	if c.conn, err = smtp.Dial(c.SMTPServer); err != nil {
		return
	}

	// Secure with STARTTLS if possible
	if ok, _ := c.conn.Extension("STARTTLS"); ok {
		host, _, _ := net.SplitHostPort(c.SMTPServer)
		if err = c.conn.StartTLS(&tls.Config{ServerName: host}); err != nil {
			return err
		}
	}

	return
}

// Disconnect disconnects from the email server.
func (c *Client) Disconnect() error {
	if c.conn == nil {
		return nil
	}

	conn := c.conn
	c.conn = nil
	return conn.Close()
}

// SendMessage sends an email message
func (c *Client) SendMessage(msg *SingleMessage) (err error) {
	// Re-open connection if required
	if err = c.Connect(); err != nil {
		return
	}

	err = c.conn.Mail(msg.Message.Author)
	if err != nil {
		return
	}

	err = c.conn.Rcpt(msg.Recipient.Address)
	if err != nil {
		return
	}

	w, err := c.conn.Data()
	if err != nil {
		return
	}

	err = msg.Write(w)
	if err != nil {
		return
	}

	return w.Close()
}
