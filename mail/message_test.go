// Copyright 2019 Silke Hofstra
//
// Licensed under the EUPL (the "Licence");
//
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and
// limitations under the Licence.

package mail

import (
	"bytes"
	"fmt"
	"net/mail"
	"strings"
	"testing"
	"time"
)

var testList = &List{
	ID:      "test",
	Name:    "Test",
	Address: "test@example.com",
	URL:     "example.com/lists/test",
	Members: []*Recipient{
		{Name: "Alice", Address: "alice@example.com"},
		{Name: "Bob", Address: "bob@example.com"},
	},
	Hidden: true,
}

// Test message
var testMessage = &Message{
	Message: &mail.Message{
		Header: mail.Header{
			"From":     []string{"Alice <alice@example.com>"},
			"To":       []string{"Bob <bob@example.com>"},
			"Subject":  []string{"Test subject"},
			"Date":     []string{testMessageDate.Format(time.RFC822)},
			"Received": []string{"by server 1", "by server 2"},
		},
		Body: nil,
	},
	List:   testList,
	Author: "Alice <alice@example.com>",
	Date:   testMessageDate,
	Body:   []byte("Test message body"),
}

// ID of test message
const testMessageID = "cad6e1419a8094ed"

// Date of test message
var testMessageDate = time.Date(2000, 1, 2, 3, 4, 0, 0, time.UTC)

func TestMessage_SingleMessages(t *testing.T) {
	singleMessages := testMessage.SingleMessages()
	for i, exp := range testList.Members {
		rcpt := singleMessages[i].Recipient
		if rcpt != exp {
			t.Errorf("Incorrect recipient for single message %v: expected %q got %q",
				i, exp, rcpt)
		}
	}
}

func TestMessage_ID(t *testing.T) {
	if testMessage.ID() != testMessageID {
		t.Errorf("Incorrect message ID: expected %s, got %s",
			testMessageID, testMessage.ID())
	}
}

func TestMessage_FileName(t *testing.T) {
	prefix := fmt.Sprintf("%v.%s.",
		testMessageDate.Unix(), testMessageID)
	if !strings.HasPrefix(testMessage.FileName(), prefix) {
		t.Errorf("Incorrect file name: expected %s<hostname>, got %s",
			prefix, testMessage.FileName())
	}
}

func TestMessage_SetBody(t *testing.T) {
	// Serialize message
	out := new(strings.Builder)
	err := testMessage.WriteRaw(out)
	if err != nil {
		t.Errorf("Error serializing message: %s", err)
		t.FailNow()
	}

	// Deserialize message
	msg := &Message{List: testList}
	err = msg.SetBody(strings.NewReader(out.String()))
	if err != nil {
		t.Errorf("Error deserializing message: %s", err)
		t.FailNow()
	}

	// Check properties
	if testMessage.Author != msg.Author {
		t.Errorf("Incorrect author: expected %q, got %q", testMessage.Author, msg.Author)
	}
	if testMessage.Date != msg.Date {
		t.Errorf("Incorrect date: expected %s, got %s", testMessage.Date, msg.Date)
	}
	if !bytes.Equal(testMessage.Body, msg.Body) {
		t.Errorf("Incorrect body: expected %q, got %q", testMessage.Body, msg.Body)
	}
}

func TestMessage_WriteRaw(t *testing.T) {
	// Serialize message
	out := new(strings.Builder)
	err := testMessage.WriteRaw(out)
	if err != nil {
		t.Errorf("Error serializing message: %s", err)
		t.FailNow()
	}

	// Deserialize message
	msg, err := mail.ReadMessage(strings.NewReader(out.String()))
	if err != nil {
		t.Errorf("Error deserializing message: %s", err)
		t.FailNow()
	}

	// Check header count
	if len(msg.Header) != len(testMessage.Header) {
		t.Errorf("Unexpected header count: expected %v, got %v",
			len(testMessage.Header), len(msg.Header))
	}

	// Check header content
	for h := range msg.Header {
		got := msg.Header.Get(h)
		exp := testMessage.Header.Get(h)

		if exp != got {
			t.Errorf("Incorrect header value for %q: expected %q, got %q",
				h, exp, got)
		}
	}
}

func TestMessage_Write(t *testing.T) {
	t.Logf("Not yet implemented")
}

func TestMessage_writeHeaders(t *testing.T) {
	t.Logf("Not yet implemented")
}

// Tests for headers
var headerTests = []struct{ In, Out string }{
	{"from: alice@example.com", "From: alice@example.com"},
	{"to: <test@example.com>", "To: Alice <alice@example.com>"},

	// Other headers
	{"mime-version: 1.0", "MIME-Version: 1.0"},

	// Mailing list headers that should be stripped
	{"Precedence: ignored", ""},
	{"X-Loop: ignored", ""},
	{"List-Id: ignored", ""},
	{"List-Archive: ignored", ""},
	{"List-Owner: ignored", ""},
	{"List-Help: ignored", ""},
	{"List-Subscribe: ignored", ""},
	{"List-Unsubscribe: ignored", ""},
	{"List-Post: ignored", ""},

	// Subject tests
	{"subject: test message", "Subject: [Test] test message"},
	{"subject: Subject", "Subject: [Test] Subject"},
	{"subject: [Test] Subject", "Subject: [Test] Subject"},
	{"subject: Re: [Test] Subject", "Subject: Re: [Test] Subject"},
	{"subject: RE: [Test] Subject", "Subject: RE: [Test] Subject"},
	{"subject: Fwd: [Test] Subject", "Subject: Fwd: [Test] Subject"},
	{"subject: FWD: [Test] Subject", "Subject: FWD: [Test] Subject"},
	{"subject: Re: [Other] Subject", "Subject: [Test] Re: [Other] Subject"},
}

func TestMessage_writeMessageHeaders(t *testing.T) {
	// Test message
	msg := &Message{
		Message: new(mail.Message),
		List:    testList,
	}

	// Output buffer
	out := new(strings.Builder)

	// Test all headers
	for _, test := range headerTests {
		out.Reset()

		var err error
		msg.Message, err = mail.ReadMessage(strings.NewReader(test.In + "\r\n\r\n"))
		if err != nil {
			t.Errorf("Error parsing header %q: %s", test.In, err)
			t.FailNow()
		}

		err = msg.writeMessageHeaders(out, testList.Members[0])
		if err != nil {
			t.Errorf("Error serializing header %q: %s", test.In, err)
			t.FailNow()
		}

		outStr := strings.TrimSpace(out.String())
		if outStr != test.Out {
			t.Errorf("Incorrect serialization for %q: expected %q, got %q",
				test.In, test.Out, outStr)
		}
	}
}
