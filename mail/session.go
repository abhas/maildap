// Copyright 2019 Silke Hofstra
//
// Licensed under the EUPL (the "Licence");
//
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and
// limitations under the Licence.

package mail

import (
	"fmt"
	"io"
	"log"

	"github.com/emersion/go-smtp"
)

// Session contains the sessions information for a connection.
// It implements smtp.Session.
type Session struct {
	backend *Backend
	message *Message
}

// Mail handles the MAIL FROM command.
func (s *Session) Mail(from string) error {
	s.message = &Message{
		Author: from,
	}
	return nil
}

// Rcpt handles the RCPT TO command.
func (s *Session) Rcpt(to string) error {
	// Check if list exists
	list := s.backend.GetList(to)
	if list == nil {
		log.Printf("Unknown list for: %q", to)
		return smtp.ErrAuthRequired
	}

	if s.message != nil {
		s.message.List = list
	}

	return nil
}

// Data handles the DATA command,
// and the end of the message.
func (s *Session) Data(r io.Reader) error {
	// Attempt to read the body, or return a temporary error
	err := s.message.SetBody(r)
	if err != nil {
		return temporaryError(err)
	}

	// Log the reception
	log.Printf("Received message on list %q from %q with subject %q",
		s.message.List.Name, s.message.Author, s.message.Header.Get("Subject"))

	// Loop detection
	if s.message.Header.Get("X-Loop") == fmt.Sprintf("<%s>", s.message.List.Address) {
		log.Printf("Discard because of loop detection")
		return nil
	}

	// Add the message to send queue
	err = (*s.backend.HandleFunc)(s.message)
	if err != nil {
		return temporaryError(err)
	}
	return nil
}

// Reset discards the currently processed message.
func (s *Session) Reset() {
	s.message = nil
}

// Logout frees all resources associated with session.
func (s *Session) Logout() error {
	return nil
}
