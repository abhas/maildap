// Copyright 2019 Silke Hofstra
//
// Licensed under the EUPL (the "Licence");
//
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and
// limitations under the Licence.

package ldap

// Config represents the configuration for an LDAP client.
type Config struct {
	// URL, Username and Password contain the connection URL and credentials for connecting to the LDAP server.
	URL, Username, Password string

	// StartTLS enables StartTLS on the connection.
	StartTLS bool

	// GroupBase contains the base in which groups are searched.
	GroupBase string `yaml:"group_base"`

	// UserBase contains the base in which users are searched.
	UserBase string `yaml:"user_base"`
}
